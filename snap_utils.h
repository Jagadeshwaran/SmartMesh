
// (c) Copyright 2016 Synapse Wireless, Inc.

// IMPORTANT NOTE:  This file is automatically generated.  Any modifications
//                  WILL BE LOST if you rerun snapbuild.

#ifndef __builtin_h_
#define __builtin_h_

#include "snappy.h"

extern uint16_t ( * random12)( uint8_t nargs );
#define random() random12(0)

extern void ( * writePin)( const uint8_t pin, uint8_t isHigh );

extern void ( * pulsePin )( uint8_t pin, uint16_t millis, uint8_t isPositive );

extern uint8_t ( * readPin )( const uint8_t pin );

extern const uint8_t DECL_GEN * ( * strObject )( py_obj const DECL_GEN *obj );

extern uint8_t ( * pyerr ) ( uint8_t errcode );

#endif // __builtin_h_
