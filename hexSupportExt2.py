"""
Common hexadecimal utilities
"""

    
def hexNibble(nibble):
    '''Convert a numeric nibble 0x0-0xF to its ASCII string representation "0"-"F"'''
    hexStr = "0123456789ABCDEF"
    return hexStr[nibble & 0xF]

def printHex(byte):
    '''print a byte in hex - input is an integer, not a string'''
    print hexNibble(byte >> 4),
    print hexNibble(byte),         # no trailing CR/LF
    
def byteToHex(byte):
    '''print a byte in hex - input is an integer, not a string'''
    upper = hexNibble(byte >> 4)
    lower = hexNibble(byte)
    return upper + lower

def printHexWord(word):
    '''print a word in hex - input is an integer, not a string'''
    printHex(word >> 8)
    printHex(word & 0xff)

def dumpHex(str):
    '''dump a string of bytes in hex'''
    count = len(str)
    index = 0
    while index < count:
        printHex(ord(str[index]))
        index += 1
        if (index & 15) == 0:
            print
        else:
            print ' ',
    print

def convBinToStr(str):
    '''Convert a string of bytes in hex
       Returns "AABBCC"'''
    index = 0
    bigStr = ''
    while index < len(str):
        bigStr += byteToHex(ord(str[index]))
        index += 1
    return bigStr

#----------------------------- Reverse Direction ------------------------
def convStrNibble(nibble):
    '''Convert the nibble to hex value'''
    
    strToCheck = "0123456789"
    i = 0 
    found = False
    while i < len(strToCheck):
        if nibble == strToCheck[i]:
            found = True
            break
        i+=1
    if found:
        return (ord(nibble) - ord('0'))
        
    
    strToCheck = "ABCDEF"
    i = 0
    found = False
    while i < len(strToCheck):
        if nibble == strToCheck[i]:
            found = True
            break
        i+=1
    if found:
        return (ord(nibble) - ord('A') + 10)
        
    
    strToCheck = "abcdef"
    i = 0
    found = False
    while i < len(strToCheck):
        if nibble == strToCheck[i]:
            found = True
            break
        i+=1
    if found:
        return (ord(nibble) - ord('a') + 10)
        
    return None

def convStrToByte(byteStr):
    '''Convert a byte-sized string'''
    result = convStrNibble(byteStr[0]) << 4
    result += convStrNibble(byteStr[1])
    return result
    
def convStrToAddr(addrStr):
    '''Convert a regular string into a SNAP address
        Expects 'AABBCC' address format as input'''
    if len(addrStr) >= 6:
        return chr(convStrToByte(addrStr[0:2])) + chr(convStrToByte(addrStr[2:4])) + chr(convStrToByte(addrStr[4:6]))
        
def convStrToMac(fullMac):
    if len(fullMac) == 16:
        convMac = convStrToAddr(fullMac[0:6]) + convStrToAddr(fullMac[6:12]) + chr(convStrToByte(fullMac[12:14])) + chr(convStrToByte(fullMac[14:]))
        
