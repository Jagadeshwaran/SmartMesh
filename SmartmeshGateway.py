# where ever #*** It means TBD 

import tornado
from snapconnect_futures import SnapConnectFutures
from snapconnect import snap
from tornado.gen import coroutine
import apy
import time
import datetime
import sqlite3 # Database for E20
import subprocess
import os
#*** This may not be the actual part of production code
import mysql.connector #Database for AWS cloud

FirstTime = True
PushConfig = False
NoOfNodesCounter = 0


def Ackknowledgement(x):
	#*** It is to ensure whether the acknowledgment is received 
	print "Ackknowledgementreceived",x
	
def SampledData(x,y,v):
	now = datetime.datetime.now()
	print "Current Date and Time is  :-->", str(now)
	print ("The sampled ADC data of address %s is %s No %s:" %(x,v,y))
	AdddataTosqliteDatabase(now, x, v, y, 9, 9, 9, 9)

#---------------------------------------------------------------#
# All the mesh nodes are stored here in the Node address list,
# nodes reply back their address after hearing to the gateway msg
#---------------------------------------------------------------#
#*** This has to be fixed based on UI. It can be a table in database also
NodeAddress = [ ]

def MeshNodes(x):
	global NoOfNodesCounter, PushConfig
	if (NoOfNodesCounter == 0):
		NodeAddress.append(x)
		NoOfNodesCounter +=1
		#PushConfig = True
		#ReportSampledValuesatThePeriod(x,24,0,59)
	#*** The received node address is added to a list
	#NodeAddress.append(x)
	print "The Receved Address is:", x
	#*** There is a probability that the nodes may miss the ack since it becomes a blocking call
	#ReportSampledValuesatThePeriod(x,24,0,5)
	#print "The No of nodes in the SmartMesh network are:", NoOfNodesCounter
	#*** An algorithm should be developed to find an appropriate value to reach the nodes back with configuration from user, it can be either time or number of nodes count
	# Its the right time to push configuration
	if NoOfNodesCounter > 0: 
		if x in NodeAddress:
			print "Address already exixts"
			print "The total number of nodes in smart mesh network are",NoOfNodesCounter
		else:
			NodeAddress.append(x)
			print "pushing the configuration......"
			#scf.dmcast_rpc(x,"ReportingPeriod",24,0,5)
			#PushConfig = True
			NoOfNodesCounter +=1
			print "The address added to list is", x
			print "The total number of nodes in smart mesh network are",NoOfNodesCounter
	"""	
	for NodiID in xrange(NoOfNodesCounter):	
		if(x != NodeAddress[NodiID]):
			NodeAddress.append(x)
			NoOfNodesCounter +=1
			print "The address added to list is", x
			print "The total number of nodes in smart mesh network are",NoOfNodesCounter
			#ReportSampledValuesatThePeriod(x,24,0,5)
		else:
			print "Address already exixts"
			print "The total number of nodes in smart mesh network are",NoOfNodesCounter """
	
	#PushConfig = False
	if(NoOfNodesCounter == 8):
		PushConfig = True
		print NodeAddress
	
	
# Required IO loop setup:
scheduler = apy.ioloop_scheduler.IOLoopScheduler.instance() # Create an apy IOLoopScheduler
# Create a SNAP Connect Instance that uses the apy scheduler
comm = snap.Snap(scheduler=scheduler, funcs={'Ackknowledgement':Ackknowledgement, 'SampledData':SampledData, 'MeshNodes':MeshNodes })
#Setup tornado's IO loop by scheduling calls to SNAP Connect's poll_internals method
tornado.ioloop.PeriodicCallback(comm.poll_internals, 5).start() 
# Create an SCF instanallbacks automatically:
scf = SnapConnectFutures(comm)	

def get_module_type(module_addr):
	for x in xrange(3):
		try:
			module_type = subprocess.check_output(['snap', 'node', module_addr, 'module_type'])
			return module_type
		except subprocess.CalledProcessError:
			pass
	return False

def upgrade(module_addr, module_type):
	print 'Attempting upgrade...'
	for x in xrange(5):
		if 'module=RF200' in module_type:
			return_code = subprocess.call(['snap', 'node', module_addr, 'firmware', 'load', 'RF200_AES128_SnapV2.6.9.sfi'])
			if return_code == 0:
				return
		elif 'module=SM200' in module_type:
			return_code = subprocess.call(['snap', 'node', module_addr, 'firmware', 'load', 'SM200_AES128_SnapV2.6.9.sfi'])
			if return_code == 0:
				return
		elif 'module=SM220' in module_type:
			return_code = subprocess.call(['snap', 'node', module_addr, 'firmware', 'load', 'SM220UF1_AES128_SnapV2.6.9.sfi'])
			if return_code == 0:
				return
		elif 'module=SN220' in module_type:
			return_code = subprocess.call(['snap', 'node', module_addr, 'firmware', 'load', 'SM220UF1_AES128_SnapV2.6.9.sfi'])
			if return_code == 0:
				return
		else:
			return
	with open("issue_nodes.txt", "a") as myfile:
				myfile.write(module_addr + ' Cannot upgrade node\n')

def Commisioning():
	if os.path.isfile('issue_nodes.txt'):
		os.remove('issue_nodes.txt')
	
	with open('system_mesh_board_2.txt') as file:
		modules = file.read().splitlines()
		
	for module_addr in modules:
		print 'Working on... ' + module_addr
		# Attempt to talk to the node, if not, try and find and move it
		ping_status = subprocess.call(['snap', 'node', module_addr, 'ping'])
		if ping_status != 0:
			print 'Cannot ping device, attempting to move...'
			move_status = subprocess.call(['snap', 'mesh', 'find', module_addr, '-m', 'systest'])
			print 'move_status code = ' + str(move_status)
			if move_status != 0:
				print 'Can\'t move the node.  On to the next node...'
				with open("issue_nodes.txt", "a") as myfile:
					myfile.write(module_addr + ': Cannot reach node\n')
				continue
		
		# Get the nodes module type to get its type and firmware version
		module_type = get_module_type(module_addr)
		if not module_type:
			print 'Failed to get module type'
			with open("issue_nodes.txt", "a") as myfile:
					myfile.write(module_addr + ': Cannot get module type\n')
			continue
		print module_type
		
		# Attempt to upgrade the node if it isn't running the desired version
		if 'core=2.6.9' not in module_type:
			upgrade(module_addr, module_type)

# Establish a connection with AWS server 
cnx = mysql.connector.connect(user='SmartMeshINDIA', password='Feb12016', host='smartmeshindia.czfxuyc82r5v.ap-southeast-1.rds.amazonaws.com', database='smartmeshDB', port=3306)
# create a cursor for AWS cloud
cursor = cnx.cursor()  

# Establish a connection with existing database 
conn = sqlite3.connect('SensorDatas2.db') 
# create a cursor for the database in E20 
c = conn.cursor() 

#------------------------------------------------------------#
# SensorDatas: MYSQL database server holds the all sensor datas 
#-----------------------------------------------------------#
def MySQLDatabase():
	# *** It should be able to check, when there is one already, it should skip creating one
	create_sensor_table = ("CREATE TABLE SmartMeshSensorDBINDIA2 (TimeStamp DATETIME, NodeAddress VARCHAR(30) NOT NULL, sensor1 INT(6),sensor2 INT(6), sensor3 INT(6),sensor4 INT(6),sensor5 INT(6),sensor6 INT(6)); ")
	cursor.execute(create_sensor_table)
#------------------------------------------------------------#
# SensorDatas:sqlite3 database holds the all sensor datas 
#-----------------------------------------------------------#
def sqliteDatabase():
	# ****time stamp of the datas collected with dates has to be added 
	# ******** It should be able to check, when there is one already, it should skip creating one ---- It has to be added
	c.execute('''create table SensorDatas2(TimeDateStamp integer, nodeAddress integer, sensor1 integer, sensor2 integer, sensor3 integer, sensor4 integer, sensor5 integer, sensor6 integer)''')
	
def AdddataTosqliteDatabase(Ts, nodeId, S1T, S2T, S3T, S4T, S5T, S6T):
	# Datas can be on the same table it need not be organized, since data can be read based on time and recents with search 
	c.execute('insert into SensorDatas2 values(?, ?, ?, ?, ?, ?, ?, ?)',(Ts,nodeId, S1T, S2T, S3T, S4T, S5T, S6T))
	# Save (commit) the changes
	conn.commit()
	AdddataToMySQLDatabase(Ts,nodeId, S1T, S2T, S3T, S4T, S5T, S6T)
	
def AdddataToMySQLDatabase(TS, nodeId, S1T, S2T, S3T, S4T, S5T, S6T):
	cursor.execute('insert into SmartMeshSensorDBINDIA2 values(%s ,%s, %s, %s, %s, %s, %s, %s)',(TS, nodeId, S1T, S2T, S3T, S4T, S5T, S6T))
	cnx.commit()
	#cursor.close()
	#cnx.close()
	

@coroutine
def setup_serial():
	"""This function won't return until the serial connection is established."""
	print "Connecting to serial..."
	# Since Windows counts from COM1, 0 is COM1, 1 is COM2, etc. On Linux/OS X, use something like "/dev/ttyS1"
	com_port =  '/dev/snap1' 
	bridge_addr = yield scf.open_serial(snap.SERIAL_TYPE_RS232, com_port)
	print "Connected to: ", bridge_addr
	
@coroutine
def McastTheAddress():
	print "Probing the module..."
	# returns 16 characters, that is 8 bytes each char takes 4 bits
	addr = comm.load_nv_param(2) 
	address = HexAddyToStr( addr )
	mcast_rpc_success = yield scf.mcast_rpc("ImGateway", (address) )# => True
	print "Is Mcast_rpc Sent: ", mcast_rpc_success
	
@coroutine
def ReportSampledValuesatThePeriod(x,H,M,S):
	#call back RPC ,return TRUE from node and check 
	callback_rpc_success = yield scf.callback_rpc(x, "ReportingPeriod",(H,M,S)) 	
	print "Is callback Unicastrpc Sent: ", callback_rpc_success
	
def HexAddyToStr( beaconAddrHexStr ):
    hexStr = "0123456789ABCDEF"
    beaconAddrPrintStr = ''
    count = len(beaconAddrHexStr)
    index = 0
    for index in range(5,8):
        byte = ord(beaconAddrHexStr[index])
        hexNibble1 = byte >> 4
        hexNibble2 = byte
        hexChar1 = hexStr[hexNibble1 & 0xF]
        hexChar2 = hexStr[hexNibble2 & 0xF]
        beaconAddrPrintStr = beaconAddrPrintStr + hexChar1 + hexChar2
        #index += 1
        #if index < count:
        #   beaconAddrPrintStr += '.' 
    #print "count",count
    return beaconAddrPrintStr	
	
@coroutine
def main():
	global FirstTime, PushConfig,NoOfNodesCounter
	if FirstTime == True:
		# Establish a serial communication
		yield setup_serial() 
		# Broad cast a ping
		#yield McastTheAddress() 
		# *** A daemon should be developed for Executing this only once
		#MySQLDatabase()
		#sqliteDatabase()
		#addr = snap.NV_MAC_ADDR_ID
		#print "The Local address is: %02X.%02X.%02X" % (ord(addr[-3]), ord(addr[-2]), ord(addr[-1]))
		FirstTime = False
		print "Done!"
		
	"""if PushConfig == True:
		yield ReportSampledValuesatThePeriod(NodeAddress[NoOfNodesCounter-1],24,0,5)
		PushConfig = False
		print "The address in main loop is:",NoOfNodesCounter """
	
	if PushConfig == True:
		for NodiID in xrange(NoOfNodesCounter):
			yield ReportSampledValuesatThePeriod(NodeAddress[NodiID],24,0,5)
			#scf.dmcast_rpc(NodeAddress[NodiID],"ReportingPeriod",24,0,5)
			time.sleep(0.005)
			#yield ReportSampledValuesatThePeriod(24,0,5)
		PushConfig = False
	"""now1 = datetime.datetime.now()
	print "Current Date and Time is  :-->", str(now1)"""
		
while(True):	
	tornado.ioloop.IOLoop.current().run_sync(main)
	time.sleep(0.001)