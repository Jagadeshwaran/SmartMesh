/****************************************************************************
 **             - iom128rfa1.h -
 **
 **     This file was automatically generated from the part XML file
 **     ATmega128RFA1.xml.
 **     Used with iccAVR and aAVR.
 **
 ***************************************************************************/

#ifdef  __IAR_SYSTEMS_ICC__
#ifndef _SYSTEM_BUILD
#pragma system_include
#endif
#endif

#include "iomacro.h"

#if TID_GUARD(3)
#error This file should only be compiled with iccavr or aavr with processor option -v3
#endif /* TID_GUARD(3) */

#ifndef __IOM128RFA1_H

/* SFRs are local in assembler modules (so this file may need to be */
/* included in more than one module in the same source file), */
/* but #defines must only be made once per source file. */

/*==============================*/
/* Interrupt Vector Definitions */
/*==============================*/

/* NB! vectors are specified as byte addresses */

#define RESET_vect             (0x00) /* External Pin,Power-on Reset,Brown-out Reset,Watchdog Reset,and JTAG AVR Reset. See Datasheet. */
#define INT0_vect              (0x04) /* External Interrupt Request 0 */
#define INT1_vect              (0x08) /* External Interrupt Request 1 */
#define INT2_vect              (0x0C) /* External Interrupt Request 2 */
#define INT3_vect              (0x10) /* External Interrupt Request 3 */
#define INT4_vect              (0x14) /* External Interrupt Request 4 */
#define INT5_vect              (0x18) /* External Interrupt Request 5 */
#define INT6_vect              (0x1C) /* External Interrupt Request 6 */
#define INT7_vect              (0x20) /* External Interrupt Request 7 */
#define PCINT0_vect            (0x24) /* Pin Change Interrupt Request 0 */
#define PCINT1_vect            (0x28) /* Pin Change Interrupt Request 1 */
#define PCINT2_vect            (0x2C) /* Pin Change Interrupt Request 2 */
#define WDT_vect               (0x30) /* Watchdog Time-out Interrupt */
#define TIMER2_COMPA_vect      (0x34) /* Timer/Counter2 Compare Match A */
#define TIMER2_COMPB_vect      (0x38) /* Timer/Counter2 Compare Match B */
#define TIMER2_OVF_vect        (0x3C) /* Timer/Counter2 Overflow */
#define TIMER1_CAPT_vect       (0x40) /* Timer/Counter1 Capture Event */
#define TIMER1_COMPA_vect      (0x44) /* Timer/Counter1 Compare Match A */
#define TIMER1_COMPB_vect      (0x48) /* Timer/Counter1 Compare Match B */
#define TIMER1_COMPC_vect      (0x4C) /* Timer/Counter1 Compare Match C */
#define TIMER1_OVF_vect        (0x50) /* Timer/Counter1 Overflow */
#define TIMER0_COMPA_vect      (0x54) /* Timer/Counter0 Compare Match A */
#define TIMER0_COMPB_vect      (0x58) /* Timer/Counter0 Compare Match B */
#define TIMER0_OVF_vect        (0x5C) /* Timer/Counter0 Overflow */
#define SPI_STC_vect           (0x60) /* SPI Serial Transfer Complete */
#define USART0_RX_vect         (0x64) /* USART0, Rx Complete */
#define USART0_UDRE_vect       (0x68) /* USART0 Data register Empty */
#define USART0_TX_vect         (0x6C) /* USART0, Tx Complete */
#define ANALOG_COMP_vect       (0x70) /* Analog Comparator */
#define ADC_vect               (0x74) /* ADC Conversion Complete */
#define EE_READY_vect          (0x78) /* EEPROM Ready */
#define TIMER3_CAPT_vect       (0x7C) /* Timer/Counter3 Capture Event */
#define TIMER3_COMPA_vect      (0x80) /* Timer/Counter3 Compare Match A */
#define TIMER3_COMPB_vect      (0x84) /* Timer/Counter3 Compare Match B */
#define TIMER3_COMPC_vect      (0x88) /* Timer/Counter3 Compare Match C */
#define TIMER3_OVF_vect        (0x8C) /* Timer/Counter3 Overflow */
#define USART1_RX_vect         (0x90) /* USART1, Rx Complete */
#define USART1_UDRE_vect       (0x94) /* USART1 Data register Empty */
#define USART1_TX_vect         (0x98) /* USART1, Tx Complete */
#define TWI_vect               (0x9C) /* 2-wire Serial Interface */
#define SPM_READY_vect         (0xA0) /* Store Program Memory Read */
#define TIMER4_CAPT_vect       (0xA4) /* Timer/Counter4 Capture Event */
#define TIMER4_COMPA_vect      (0xA8) /* Timer/Counter4 Compare Match A */
#define TIMER4_COMPB_vect      (0xAC) /* Timer/Counter4 Compare Match B */
#define TIMER4_COMPC_vect      (0xB0) /* Timer/Counter4 Compare Match C */
#define TIMER4_OVF_vect        (0xB4) /* Timer/Counter4 Overflow */
#define TIMER5_CAPT_vect       (0xB8) /* Timer/Counter5 Capture Event */
#define TIMER5_COMPA_vect      (0xBC) /* Timer/Counter5 Compare Match A */
#define TIMER5_COMPB_vect      (0xC0) /* Timer/Counter5 Compare Match B */
#define TIMER5_COMPC_vect      (0xC4) /* Timer/Counter5 Compare Match C */
#define TIMER5_OVF_vect        (0xC8) /* Timer/Counter5 Overflow */
#define USART2_RX_vect         (0xCC) /* USART2, Rx Complete */
#define USART2_UDRE_vect       (0xD0) /* USART2 Data register Empty */
#define USART2_TX_vect         (0xD4) /* USART2, Tx Complete */
#define USART3_RX_vect         (0xD8) /* USART3, Rx Complete */
#define USART3_UDRE_vect       (0xDC) /* USART3 Data register Empty */
#define USART3_TX_vect         (0xE0) /* USART3, Tx Complete */
#define TRX24_PLL_LOCK_vect    (0xE4) /* TRX24 - PLL lock interrupt */
#define TRX24_PLL_UNLOCK_vect  (0xE8) /* TRX24 - PLL unlock interrupt */
#define TRX24_RX_START_vect    (0xEC) /* TRX24 - Receive start interrupt */
#define TRX24_RX_END_vect      (0xF0) /* TRX24 - RX_END interrupt */
#define TRX24_CCA_ED_DONE_vect (0xF4) /* TRX24 - CCA/ED done interrupt */
#define TRX24_XAH_AMI_vect     (0xF8) /* TRX24 - XAH - AMI */
#define TRX24_TX_END_vect      (0xFC) /* TRX24 - TX_END interrupt */
#define TRX24_AWAKE_vect       (0x100) /* TRX24 AWAKE - tranceiver is reaching state TRX_OFF */
#define SCNT_CMP1_vect         (0x104) /* Symbol counter - compare match 1 interrupt */
#define SCNT_CMP2_vect         (0x108) /* Symbol counter - compare match 2 interrupt */
#define SCNT_CMP3_vect         (0x10C) /* Symbol counter - compare match 3 interrupt */
#define SCNT_OVFL_vect         (0x110) /* Symbol counter - overflow interrupt */
#define SCNT_BACKOFF_vect      (0x114) /* Symbol counter - backoff interrupt */
#define AES_READY_vect         (0x118) /* AES engine ready interrupt */
#define BAT_LOW_vect           (0x11C) /* Battery monitor indicates supply voltage below threshold */


#ifdef __IAR_SYSTEMS_ASM__   
#ifndef ENABLE_BIT_DEFINITIONS
#define  ENABLE_BIT_DEFINITIONS
#endif /* ENABLE_BIT_DEFINITIONS */
#include "bitdefinitions/iom128rfa1.inc"
#endif /* __IAR_SYSTEMS_ASM__ */

#endif /* __IOM128RFA1_H (define part) */

/* Include the SFR part if this file has not been included before,
 * OR this file is included by the assembler (SFRs must be defined in
 * each assembler module). */
#if !defined(__IOM128RFA1_H) || defined(__IAR_SYSTEMS_ASM__)
#define __IOM128RFA1_H

#pragma language=save
#pragma language=extended

/*==========================*/
/* Predefined SFR Addresses */
/*==========================*/

/****************************************************************************
 * An example showing the SFR_B() macro call,
 * the expanded result and usage of this result:
 *
 * SFR_B_R(0x1F,   AVR) Expands to:
 * __io union {
 *             unsigned char AVR;                 // The sfrb as 1 byte
 *             struct {                           // The sfrb as 8 bits
 *                     unsigned char AVR_Bit0:1,
 *                                   AVR_Bit1:1,
 *                                   AVR_Bit2:1,
 *                                   AVR_Bit3:1,
 *                                   AVR_Bit4:1,
 *                                   AVR_Bit5:1,
 *                                   AVR_Bit6:1,
 *                                   AVR_Bit7:1;
 *                    };
 *            } @ 0x1F;
 * Examples of how to use the expanded result:
 * AVR |= (1<<5);
 * or like this:
 * AVR_Bit5 = 1;
 *
 *
 *
 * An example showing the SFR_B_N() macro call, 
 * the expanded result and usage of this result:
 * SFR_B_N(0x25,  TCCR2, FOC2, WGM20, COM21, COM20, WGM21, CS22, CS21, CS20)
 *  Expands to:
 *  __io union { 
 *              unsigned char TCCR2; 
 *              struct { 
 *                      unsigned char TCCR2_Bit0:1, 
 *                                    TCCR2_Bit1:1, 
 *                                    TCCR2_Bit2:1, 
 *                                    TCCR2_Bit3:1, 
 *                                    TCCR2_Bit4:1, 
 *                                    TCCR2_Bit5:1, 
 *                                    TCCR2_Bit6:1, 
 *                                    TCCR2_Bit7:1; 
 *                     }; 
 *              struct { 
 *                      unsigned char TCCR2_CS20:1, 
 *                                    TCCR2_CS21:1, 
 *                                    TCCR2_CS22:1, 
 *                                    TCCR2_WGM21:1, 
 *                                    TCCR2_COM20:1, 
 *                                    TCCR2_COM21:1, 
 *                                    TCCR2_WGM20:1, 
 *                                    TCCR2_FOC2:1; 
 *                     }; 
 *             } @ 0x25;
 * Examples of how to use the expanded result:
 * TCCR2 |= (1<<5); 
 * or if ENABLE_BIT_DEFINITIONS is defined   
 * TCCR2 |= (1<<COM21);
 * or like this:
 * TCCR2_Bit5 = 1;
 * or like this:
 * TCCR2_COM21 = 1;
 ***************************************************************************/

 SFR_B_N(0x00, PINA, PINA7, PINA6, PINA5, PINA4, PINA3, PINA2, PINA1, PINA0)
 SFR_B_N(0x01, DDRA, DDA7, DDA6, DDA5, DDA4, DDA3, DDA2, DDA1, DDA0)
 SFR_B_N(0x02, PORTA, PORTA7, PORTA6, PORTA5, PORTA4, PORTA3, PORTA2, PORTA1, PORTA0)
 SFR_B_N(0x03, PINB, PINB7, PINB6, PINB5, PINB4, PINB3, PINB2, PINB1, PINB0)
 SFR_B_N(0x04, DDRB, DDB7, DDB6, DDB5, DDB4, DDB3, DDB2, DDB1, DDB0)
 SFR_B_N(0x05, PORTB, PORTB7, PORTB6, PORTB5, PORTB4, PORTB3, PORTB2, PORTB1, PORTB0)
 SFR_B_N(0x06, PINC, PINC7, PINC6, PINC5, PINC4, PINC3, PINC2, PINC1, PINC0)
 SFR_B_N(0x07, DDRC, DDC7, DDC6, DDC5, DDC4, DDC3, DDC2, DDC1, DDC0)
 SFR_B_N(0x08, PORTC, PORTC7, PORTC6, PORTC5, PORTC4, PORTC3, PORTC2, PORTC1, PORTC0)
 SFR_B_N(0x09, PIND, PIND7, PIND6, PIND5, PIND4, PIND3, PIND2, PIND1, PIND0)
 SFR_B_N(0x0A, DDRD, DDD7, DDD6, DDD5, DDD4, DDD3, DDD2, DDD1, DDD0)
 SFR_B_N(0x0B, PORTD, PORTD7, PORTD6, PORTD5, PORTD4, PORTD3, PORTD2, PORTD1, PORTD0)
 SFR_B_N(0x0C, PINE, PINE7, PINE6, PINE5, PINE4, PINE3, PINE2, PINE1, PINE0)
 SFR_B_N(0x0D, DDRE, DDE7, DDE6, DDE5, DDE4, DDE3, DDE2, DDE1, DDE0)
 SFR_B_N(0x0E, PORTE, PORTE7, PORTE6, PORTE5, PORTE4, PORTE3, PORTE2, PORTE1, PORTE0)
 SFR_B_N(0x0F, PINF, PINF7, PINF6, PINF5, PINF4, PINF3, PINF2, PINF1, PINF0)
 SFR_B_N(0x10, DDRF, DDF7, DDF6, DDF5, DDF4, DDF3, DDF2, DDF1, DDF0)
 SFR_B_N(0x11, PORTF, PORTF7, PORTF6, PORTF5, PORTF4, PORTF3, PORTF2, PORTF1, PORTF0)
 SFR_B_N(0x12, PING, Res1, Res0, PING5, PING4, PING3, PING2, PING1, PING0)
 SFR_B_N(0x13, DDRG, Res1, Res0, DDG5, DDG4, DDG3, DDG2, DDG1, DDG0)
 SFR_B_N(0x14, PORTG, Res1, Res0, PORTG5, PORTG4, PORTG3, PORTG2, PORTG1, PORTG0)
 SFR_B_N(0x15, TIFR0, Res4, Res3, Res2, Res1, Res0, OCF0B, OCF0A, TOV0)
 SFR_B_N(0x16, TIFR1, Res1, Res0, ICF1, Res, OCF1C, OCF1B, OCF1A, TOV1)
 SFR_B_N(0x17, TIFR2, Res4, Res3, Res2, Res1, Res0, OCF2B, OCF2A, TOV2)
 SFR_B_N(0x18, TIFR3, Res1, Res0, ICF3, Res, OCF3C, OCF3B, OCF3A, TOV3)
 SFR_B_N(0x19, TIFR4, Res1, Res0, ICF4, Res, OCF4C, OCF4B, OCF4A, TOV4)
 SFR_B_N(0x1A, TIFR5, Res1, Res0, ICF5, Res, OCF5C, OCF5B, OCF5A, TOV5)
 SFR_B_N(0x1B, PCIFR, Res4, Res3, Res2, Res1, Res0, PCIF2, PCIF1, PCIF0)
 SFR_B_N(0x1C, EIFR, INTF7, INTF6, INTF5, INTF4, INTF3, INTF2, INTF1, INTF0)
 SFR_B_N(0x1D, EIMSK, INT7, INT6, INT5, INT4, INT3, INT2, INT1, INT0)
 SFR_B_N(0x1E, GPIOR0, GPIOR07, GPIOR06, GPIOR05, GPIOR04, GPIOR03, GPIOR02, GPIOR01, GPIOR00)
 SFR_B_N(0x1F, EECR, Res1, Res0, EEPM1, EEPM0, EERIE, EEMPE, EEPE, EERE)
 SFR_B_N(0x20, EEDR, EEDR7, EEDR6, EEDR5, EEDR4, EEDR3, EEDR2, EEDR1, EEDR0)
 SFR_W_R(0x21, EEAR)
 SFR_B_N(0x23, GTCCR, TSM, Res4, Res3, Res2, Res1, Res0, PSRASY, PSRSYNC)
 SFR_B_N(0x24, TCCR0A, COM0A1, COM0A0, COM0B1, COM0B0, Res1, Res0, WGM01, WGM00)
 SFR_B_N(0x25, TCCR0B, FOC0A, FOC0B, Res1, Res0, WGM02, CS02, CS01, CS00)
 SFR_B_N(0x26, TCNT0, TCNT0_7, TCNT0_6, TCNT0_5, TCNT0_4, TCNT0_3, TCNT0_2, TCNT0_1, TCNT0_0)
 SFR_B_N(0x27, OCR0A, OCR0A_7, OCR0A_6, OCR0A_5, OCR0A_4, OCR0A_3, OCR0A_2, OCR0A_1, OCR0A_0)
 SFR_B_N(0x28, OCR0B, OCR0B_7, OCR0B_6, OCR0B_5, OCR0B_4, OCR0B_3, OCR0B_2, OCR0B_1, OCR0B_0)
 SFR_B_N(0x2A, GPIOR1, GPIOR17, GPIOR16, GPIOR15, GPIOR14, GPIOR13, GPIOR12, GPIOR11, GPIOR10)
 SFR_B_N(0x2B, GPIOR2, GPIOR27, GPIOR26, GPIOR25, GPIOR24, GPIOR23, GPIOR22, GPIOR21, GPIOR20)
 SFR_B_N(0x2C, SPCR, SPIE, SPE, DORD, MSTR, CPOL, CPHA, SPR1, SPR0)
 SFR_B_N(0x2D, SPSR, SPIF, WCOL, Res4, Res3, Res2, Res1, Res0, SPI2X)
 SFR_B_N(0x2E, SPDR, SPDR7, SPDR6, SPDR5, SPDR4, SPDR3, SPDR2, SPDR1, SPDR0)
 SFR_B_N(0x30, ACSR, ACD, ACBG, ACO, ACI, ACIE, ACIC, ACIS1, ACIS0)
 SFR_B_N(0x31, OCDR, OCDR7, OCDR6, OCDR5, OCDR4, OCDR3, OCDR2, OCDR1, OCDR0)
 SFR_B_N(0x33, SMCR, Res3, Res2, Res1, Res0, SM2, SM1, SM0, SE)
 SFR_B_N(0x34, MCUSR, Res2, Res1, Res0, JTRF, WDRF, BORF, EXTRF, PORF)
 SFR_B_N(0x35, MCUCR, JTD, Dummy6, Dummy5, PUD, Res1, Res0, IVSEL, IVCE)
 SFR_B_N(0x37, SPMCSR, SPMIE, RWWSB, SIGRD, RWWSRE, BLBSET, PGWRT, PGERS, SPMEN)
 SFR_B_N(0x3B, RAMPZ, Res5, Res4, Res3, Res2, Res1, Res0, RAMPZ1, RAMPZ0)
 SFR_W_R(0x3D, SP)
 SFR_B_N(0x3F, SREG, I, T, H, S, V, N, Z, C)
 SFR_B_N(0x60, WDTCSR, WDIF, WDIE, WDP3, WDCE, WDE, WDP2, WDP1, WDP0)
 SFR_B_N(0x61, CLKPR, CLKPCE, Res2, Res1, Res0, CLKPS3, CLKPS2, CLKPS1, CLKPS0)
 SFR_B_N(0x63, PRR2, Res3, Res2, Res1, Res0, PRRAM3, PRRAM2, PRRAM1, PRRAM0)
 SFR_B_N(0x64, PRR0, PRTWI, PRTIM2, PRTIM0, PRPGA, PRTIM1, PRSPI, PRUSART0, PRADC)
 SFR_B_N(0x65, PRR1, Res, PRTRX24, PRTIM5, PRTIM4, PRTIM3, PRUSART3, PRUSART2, PRUSART1)
 SFR_B_N(0x66, OSCCAL, CAL7, CAL6, CAL5, CAL4, CAL3, CAL2, CAL1, CAL0)
 SFR_B_N(0x67, BGCR, Res, BGCAL_FINE3, BGCAL_FINE2, BGCAL_FINE1, BGCAL_FINE0, BGCAL2, BGCAL1, BGCAL0)
 SFR_B_N(0x68, PCICR, Res4, Res3, Res2, Res1, Res0, PCIE2, PCIE1, PCIE0)
 SFR_B_N(0x69, EICRA, ISC31, ISC30, ISC21, ISC20, ISC11, ISC10, ISC01, ISC00)
 SFR_B_N(0x6A, EICRB, ISC71, ISC70, ISC61, ISC60, ISC51, ISC50, ISC41, ISC40)
 SFR_B_N(0x6B, PCMSK0, PCINT7, PCINT6, PCINT5, PCINT4, PCINT3, PCINT2, PCINT1, PCINT0)
 SFR_B_N(0x6C, PCMSK1, PCINT15, PCINT14, PCINT13, PCINT12, PCINT11, PCINT10, PCINT9, PCINT8)
 SFR_B_N(0x6D, PCMSK2, PCINT23, PCINT22, PCINT21, PCINT20, PCINT19, PCINT18, PCINT17, PCINT16)
 SFR_B_N(0x6E, TIMSK0, Res4, Res3, Res2, Res1, Res0, OCIE0B, OCIE0A, TOIE0)
 SFR_B_N(0x6F, TIMSK1, Res1, Res0, ICIE1, Res, OCIE1C, OCIE1B, OCIE1A, TOIE1)
 SFR_B_N(0x70, TIMSK2, Res4, Res3, Res2, Res1, Res0, OCIE2B, OCIE2A, TOIE2)
 SFR_B_N(0x71, TIMSK3, Res1, Res0, ICIE3, Res, OCIE3C, OCIE3B, OCIE3A, TOIE3)
 SFR_B_N(0x72, TIMSK4, Res1, Res0, ICIE4, Res, OCIE4C, OCIE4B, OCIE4A, TOIE4)
 SFR_B_N(0x73, TIMSK5, Res1, Res0, ICIE5, Res, OCIE5C, OCIE5B, OCIE5A, TOIE5)
 SFR_B_N(0x75, NEMCR, Dummy7, ENEAM, AEAM1, AEAM0, Dummy3, Dummy2, Dummy1, Dummy0)
 SFR_B_N(0x77, ADCSRC, ADTHT1, ADTHT0, Res0, ADSUT4, ADSUT3, ADSUT2, ADSUT1, ADSUT0)
 SFR_W_R(0x78, ADC)
 SFR_B_N(0x7A, ADCSRA, ADEN, ADSC, ADATE, ADIF, ADIE, ADPS2, ADPS1, ADPS0)
 SFR_B_N(0x7B, ADCSRB, AVDDOK, ACME, REFOK, ACCH, MUX5, ADTS2, ADTS1, ADTS0)
 SFR_B_N(0x7C, ADMUX, REFS1, REFS0, ADLAR, MUX4, MUX3, MUX2, MUX1, MUX0)
 SFR_B_N(0x7D, DIDR2, ADC15D, ADC14D, ADC13D, ADC12D, ADC11D, ADC10D, ADC9D, ADC8D)
 SFR_B_N(0x7E, DIDR0, ADC7D, ADC6D, ADC5D, ADC4D, ADC3D, ADC2D, ADC1D, ADC0D)
 SFR_B_N(0x7F, DIDR1, Dummy7, Dummy6, Dummy5, Dummy4, Dummy3, Dummy2, AIN1D, AIN0D)
 SFR_B_N(0x80, TCCR1A, COM1A1, COM1A0, COM1B1, COM1B0, COM1C1, COM1C0, WGM11, WGM10)
 SFR_B_N(0x81, TCCR1B, ICNC1, ICES1, Res, WGM13, WGM12, CS12, CS11, CS10)
 SFR_B_N(0x82, TCCR1C, FOC1A, FOC1B, FOC1C, Res4, Res3, Res2, Res1, Res0)
 SFR_W_R(0x84, TCNT1)
 SFR_W_R(0x86, ICR1)
 SFR_W_R(0x88, OCR1A)
 SFR_W_R(0x8A, OCR1B)
 SFR_W_R(0x8C, OCR1C)
 SFR_B_N(0x90, TCCR3A, COM3A1, COM3A0, COM3B1, COM3B0, COM3C1, COM3C0, WGM31, WGM30)
 SFR_B_N(0x91, TCCR3B, ICNC3, ICES3, Res, WGM33, WGM32, CS32, CS31, CS30)
 SFR_B_N(0x92, TCCR3C, FOC3A, FOC3B, FOC3C, Res4, Res3, Res2, Res1, Res0)
 SFR_W_R(0x94, TCNT3)
 SFR_W_R(0x96, ICR3)
 SFR_W_R(0x98, OCR3A)
 SFR_W_R(0x9A, OCR3B)
 SFR_W_R(0x9C, OCR3C)
 SFR_B_N(0xA0, TCCR4A, COM4A1, COM4A0, COM4B1, COM4B0, COM4C1, COM4C0, WGM41, WGM40)
 SFR_B_N(0xA1, TCCR4B, ICNC4, ICES4, Res, WGM43, WGM42, CS42, CS41, CS40)
 SFR_B_N(0xA2, TCCR4C, FOC4A, FOC4B, FOC4C, Res4, Res3, Res2, Res1, Res0)
 SFR_W_R(0xA4, TCNT4)
 SFR_W_R(0xA6, ICR4)
 SFR_W_R(0xA8, OCR4A)
 SFR_W_R(0xAA, OCR4B)
 SFR_W_R(0xAC, OCR4C)
 SFR_B_N(0xB0, TCCR2A, COM2A1, COM2A0, COM2B1, COM2B0, Res1, Res0, WGM21, WGM20)
 SFR_B_N(0xB1, TCCR2B, FOC2A, FOC2B, Res1, Res0, WGM22, CS22, CS21, CS20)
 SFR_B_N(0xB2, TCNT2, TCNT27, TCNT26, TCNT25, TCNT24, TCNT23, TCNT22, TCNT21, TCNT20)
 SFR_B_N(0xB3, OCR2A, OCR2A7, OCR2A6, OCR2A5, OCR2A4, OCR2A3, OCR2A2, OCR2A1, OCR2A0)
 SFR_B_N(0xB4, OCR2B, OCR2B7, OCR2B6, OCR2B5, OCR2B4, OCR2B3, OCR2B2, OCR2B1, OCR2B0)
 SFR_B_N(0xB6, ASSR, EXCLKAMR, EXCLK, AS2, TCN2UB, OCR2AUB, OCR2BUB, TCR2AUB, TCR2BUB)
 SFR_B_N(0xB8, TWBR, TWBR7, TWBR6, TWBR5, TWBR4, TWBR3, TWBR2, TWBR1, TWBR0)
 SFR_B_N(0xB9, TWSR, TWS7, TWS6, TWS5, TWS4, TWS3, Res, TWPS1, TWPS0)
 SFR_B_N(0xBA, TWAR, TWA6, TWA5, TWA4, TWA3, TWA2, TWA1, TWA0, TWGCE)
 SFR_B_N(0xBB, TWDR, TWD7, TWD6, TWD5, TWD4, TWD3, TWD2, TWD1, TWD0)
 SFR_B_N(0xBC, TWCR, TWINT, TWEA, TWSTA, TWSTO, TWWC, TWEN, Res, TWIE)
 SFR_B_N(0xBD, TWAMR, TWAM6, TWAM5, TWAM4, TWAM3, TWAM2, TWAM1, TWAM0, Res)
 SFR_B_N(0xC0, UCSR0A, RXC0, TXC0, UDRE0, FE0, DOR0, UPE0, U2X0, MPCM0)
 SFR_B_N(0xC1, UCSR0B, RXCIE0, TXCIE0, UDRIE0, RXEN0, TXEN0, UCSZ02, RXB80, TXB80)
 SFR_B_N(0xC2, UCSR0C, UMSEL01, UMSEL00, UPM01, UPM00, USBS0, UDORD0, UCPHA0, UCPOL0)
 SFR_W_R(0xC4, UBRR0)
 SFR_B_N(0xC6, UDR0, UDR07, UDR06, UDR05, UDR04, UDR03, UDR02, UDR01, UDR00)
 SFR_B_N(0xC8, UCSR1A, RXC1, TXC1, UDRE1, FE1, DOR1, UPE1, U2X1, MPCM1)
 SFR_B_N(0xC9, UCSR1B, RXCIE1, TXCIE1, UDRIE1, RXEN1, TXEN1, UCSZ12, RXB81, TXB81)
 SFR_B_N(0xCA, UCSR1C, UMSEL11, UMSEL10, UPM11, UPM10, USBS1, UDORD1, UCPHA1, UCPOL1)
 SFR_W_R(0xCC, UBRR1)
 SFR_B_N(0xCE, UDR1, UDR17, UDR16, UDR15, UDR14, UDR13, UDR12, UDR11, UDR10)
 SFR_B_N(0xDC, SCCR0, SCRES, SCMBTS, SCEN, SCCKSEL, SCTSE, SCCMP3, SCCMP2, SCCMP1)
 SFR_B_N(0xDD, SCCR1, Res6, Res5, Res4, Res3, Res2, Res1, Res0, SCENBO)
 SFR_B_N(0xDE, SCSR, Res6, Res5, Res4, Res3, Res2, Res1, Res0, SCBSY)
 SFR_B_N(0xDF, SCIRQM, Res2, Res1, Res0, IRQMBO, IRQMOF, IRQMCP3, IRQMCP2, IRQMCP1)
 SFR_B_N(0xE0, SCIRQS, Res2, Res1, Res0, IRQSBO, IRQSOF, IRQSCP3, IRQSCP2, IRQSCP1)
 SFR_B_N(0xE1, SCCNTLL, SCCNTLL7, SCCNTLL6, SCCNTLL5, SCCNTLL4, SCCNTLL3, SCCNTLL2, SCCNTLL1, SCCNTLL0)
 SFR_B_N(0xE2, SCCNTLH, SCCNTLH7, SCCNTLH6, SCCNTLH5, SCCNTLH4, SCCNTLH3, SCCNTLH2, SCCNTLH1, SCCNTLH0)
 SFR_B_N(0xE3, SCCNTHL, SCCNTHL7, SCCNTHL6, SCCNTHL5, SCCNTHL4, SCCNTHL3, SCCNTHL2, SCCNTHL1, SCCNTHL0)
 SFR_B_N(0xE4, SCCNTHH, SCCNTHH7, SCCNTHH6, SCCNTHH5, SCCNTHH4, SCCNTHH3, SCCNTHH2, SCCNTHH1, SCCNTHH0)
 SFR_B_N(0xE5, SCBTSRLL, SCBTSRLL7, SCBTSRLL6, SCBTSRLL5, SCBTSRLL4, SCBTSRLL3, SCBTSRLL2, SCBTSRLL1, SCBTSRLL0)
 SFR_B_N(0xE6, SCBTSRLH, SCBTSRLH7, SCBTSRLH6, SCBTSRLH5, SCBTSRLH4, SCBTSRLH3, SCBTSRLH2, SCBTSRLH1, SCBTSRLH0)
 SFR_B_N(0xE7, SCBTSRHL, SCBTSRHL7, SCBTSRHL6, SCBTSRHL5, SCBTSRHL4, SCBTSRHL3, SCBTSRHL2, SCBTSRHL1, SCBTSRHL0)
 SFR_B_N(0xE8, SCBTSRHH, SCBTSRHH7, SCBTSRHH6, SCBTSRHH5, SCBTSRHH4, SCBTSRHH3, SCBTSRHH2, SCBTSRHH1, SCBTSRHH0)
 SFR_B_N(0xE9, SCTSRLL, SCTSRLL7, SCTSRLL6, SCTSRLL5, SCTSRLL4, SCTSRLL3, SCTSRLL2, SCTSRLL1, SCTSRLL0)
 SFR_B_N(0xEA, SCTSRLH, SCTSRLH7, SCTSRLH6, SCTSRLH5, SCTSRLH4, SCTSRLH3, SCTSRLH2, SCTSRLH1, SCTSRLH0)
 SFR_B_N(0xEB, SCTSRHL, SCTSRHL7, SCTSRHL6, SCTSRHL5, SCTSRHL4, SCTSRHL3, SCTSRHL2, SCTSRHL1, SCTSRHL0)
 SFR_B_N(0xEC, SCTSRHH, SCTSRHH7, SCTSRHH6, SCTSRHH5, SCTSRHH4, SCTSRHH3, SCTSRHH2, SCTSRHH1, SCTSRHH0)
 SFR_B_N(0xED, SCOCR3LL, SCOCR3LL7, SCOCR3LL6, SCOCR3LL5, SCOCR3LL4, SCOCR3LL3, SCOCR3LL2, SCOCR3LL1, SCOCR3LL0)
 SFR_B_N(0xEE, SCOCR3LH, SCOCR3LH7, SCOCR3LH6, SCOCR3LH5, SCOCR3LH4, SCOCR3LH3, SCOCR3LH2, SCOCR3LH1, SCOCR3LH0)
 SFR_B_N(0xEF, SCOCR3HL, SCOCR3HL7, SCOCR3HL6, SCOCR3HL5, SCOCR3HL4, SCOCR3HL3, SCOCR3HL2, SCOCR3HL1, SCOCR3HL0)
 SFR_B_N(0xF0, SCOCR3HH, SCOCR3HH7, SCOCR3HH6, SCOCR3HH5, SCOCR3HH4, SCOCR3HH3, SCOCR3HH2, SCOCR3HH1, SCOCR3HH0)
 SFR_B_N(0xF1, SCOCR2LL, SCOCR2LL7, SCOCR2LL6, SCOCR2LL5, SCOCR2LL4, SCOCR2LL3, SCOCR2LL2, SCOCR2LL1, SCOCR2LL0)
 SFR_B_N(0xF2, SCOCR2LH, SCOCR2LH7, SCOCR2LH6, SCOCR2LH5, SCOCR2LH4, SCOCR2LH3, SCOCR2LH2, SCOCR2LH1, SCOCR2LH0)
 SFR_B_N(0xF3, SCOCR2HL, SCOCR2HL7, SCOCR2HL6, SCOCR2HL5, SCOCR2HL4, SCOCR2HL3, SCOCR2HL2, SCOCR2HL1, SCOCR2HL0)
 SFR_B_N(0xF4, SCOCR2HH, SCOCR2HH7, SCOCR2HH6, SCOCR2HH5, SCOCR2HH4, SCOCR2HH3, SCOCR2HH2, SCOCR2HH1, SCOCR2HH0)
 SFR_B_N(0xF5, SCOCR1LL, SCOCR1LL7, SCOCR1LL6, SCOCR1LL5, SCOCR1LL4, SCOCR1LL3, SCOCR1LL2, SCOCR1LL1, SCOCR1LL0)
 SFR_B_N(0xF6, SCOCR1LH, SCOCR1LH7, SCOCR1LH6, SCOCR1LH5, SCOCR1LH4, SCOCR1LH3, SCOCR1LH2, SCOCR1LH1, SCOCR1LH0)
 SFR_B_N(0xF7, SCOCR1HL, SCOCR1HL7, SCOCR1HL6, SCOCR1HL5, SCOCR1HL4, SCOCR1HL3, SCOCR1HL2, SCOCR1HL1, SCOCR1HL0)
 SFR_B_N(0xF8, SCOCR1HH, SCOCR1HH7, SCOCR1HH6, SCOCR1HH5, SCOCR1HH4, SCOCR1HH3, SCOCR1HH2, SCOCR1HH1, SCOCR1HH0)
 SFR_B_EXT_IO_N(0x120, TCCR5A, COM5A1, COM5A0, COM5B1, COM5B0, COM5C1, COM5C0, WGM51, WGM50)
 SFR_B_EXT_IO_N(0x121, TCCR5B, ICNC5, ICES5, Res, WGM53, WGM52, CS52, CS51, CS50)
 SFR_B_EXT_IO_N(0x122, TCCR5C, FOC5A, FOC5B, FOC5C, Res4, Res3, Res2, Res1, Res0)
 SFR_W_EXT_IO_R(0x124, TCNT5)
 SFR_W_EXT_IO_R(0x126, ICR5)
 SFR_W_EXT_IO_R(0x128, OCR5A)
 SFR_W_EXT_IO_R(0x12A, OCR5B)
 SFR_W_EXT_IO_R(0x12C, OCR5C)
 SFR_B_EXT_IO_N(0x12F, LLCR, Res1, Res0, LLDONE, LLCOMP, LLCAL, LLTCO, LLSHORT, LLENCAL)
 SFR_B_EXT_IO_N(0x130, LLDRL, Res3, Res2, Res1, Res0, LLDRL3, LLDRL2, LLDRL1, LLDRL0)
 SFR_B_EXT_IO_N(0x131, LLDRH, Res2, Res1, Res0, LLDRH4, LLDRH3, LLDRH2, LLDRH1, LLDRH0)
 SFR_B_EXT_IO_N(0x132, DRTRAM3, Res1, Res0, DRTSWOK, ENDRT, Dummy3, Dummy2, Dummy1, Dummy0)
 SFR_B_EXT_IO_N(0x133, DRTRAM2, Dummy7, Res, DRTSWOK, ENDRT, Dummy3, Dummy2, Dummy1, Dummy0)
 SFR_B_EXT_IO_N(0x134, DRTRAM1, Res1, Res0, DRTSWOK, ENDRT, Dummy3, Dummy2, Dummy1, Dummy0)
 SFR_B_EXT_IO_N(0x135, DRTRAM0, Res1, Res0, DRTSWOK, ENDRT, Dummy3, Dummy2, Dummy1, Dummy0)
 SFR_B_EXT_IO_N(0x136, DPDS0, PFDRV1, PFDRV0, PEDRV1, PEDRV0, PDDRV1, PDDRV0, PBDRV1, PBDRV0)
 SFR_B_EXT_IO_N(0x137, DPDS1, Res5, Res4, Res3, Res2, Res1, Res0, PGDRV1, PGDRV0)
 SFR_B_EXT_IO_N(0x139, TRXPR, Res3, Res2, Res1, Res0, Dummy3, Dummy2, SLPTR, TRXRST)
 SFR_B_EXT_IO_N(0x13C, AES_CTRL, AES_REQUEST, Dummy6, AES_MODE, Res, AES_DIR, AES_IM, Res1, Res0)
 SFR_B_EXT_IO_N(0x13D, AES_STATUS, AES_ER, Res5, Res4, Res3, Res2, Res1, Res0, AES_DONE)
 SFR_B_EXT_IO_N(0x13E, AES_STATE, AES_STATE7, AES_STATE6, AES_STATE5, AES_STATE4, AES_STATE3, AES_STATE2, AES_STATE1, AES_STATE0)
 SFR_B_EXT_IO_N(0x13F, AES_KEY, AES_KEY7, AES_KEY6, AES_KEY5, AES_KEY4, AES_KEY3, AES_KEY2, AES_KEY1, AES_KEY0)
 SFR_B_EXT_IO_N(0x141, TRX_STATUS, CCA_DONE, CCA_STATUS, TST_STATUS, TRX_STATUS4, TRX_STATUS3, TRX_STATUS2, TRX_STATUS1, TRX_STATUS0)
 SFR_B_EXT_IO_N(0x142, TRX_STATE, TRAC_STATUS2, TRAC_STATUS1, TRAC_STATUS0, TRX_CMD4, TRX_CMD3, TRX_CMD2, TRX_CMD1, TRX_CMD0)
 SFR_B_EXT_IO_N(0x143, TRX_CTRL_0, Res7, Res6, Res5, Res4, Res3, Res2, Res1, Res0)
 SFR_B_EXT_IO_N(0x144, TRX_CTRL_1, PA_EXT_EN, IRQ_2_EXT_EN, TX_AUTO_CRC_ON, Res4, Res3, Res2, Res1, Res0)
 SFR_B_EXT_IO_N(0x145, PHY_TX_PWR, PA_BUF_LT1, PA_BUF_LT0, PA_LT1, PA_LT0, TX_PWR3, TX_PWR2, TX_PWR1, TX_PWR0)
 SFR_B_EXT_IO_N(0x146, PHY_RSSI, RX_CRC_VALID, RND_VALUE1, RND_VALUE0, RSSI4, RSSI3, RSSI2, RSSI1, RSSI0)
 SFR_B_EXT_IO_N(0x147, PHY_ED_LEVEL, ED_LEVEL7, ED_LEVEL6, ED_LEVEL5, ED_LEVEL4, ED_LEVEL3, ED_LEVEL2, ED_LEVEL1, ED_LEVEL0)
 SFR_B_EXT_IO_N(0x148, PHY_CC_CCA, CCA_REQUEST, CCA_MODE1, CCA_MODE0, CHANNEL4, CHANNEL3, CHANNEL2, CHANNEL1, CHANNEL0)
 SFR_B_EXT_IO_N(0x149, CCA_THRES, CCA_CS_THRES3, CCA_CS_THRES2, CCA_CS_THRES1, CCA_CS_THRES0, CCA_ED_THRES3, CCA_ED_THRES2, CCA_ED_THRES1, CCA_ED_THRES0)
 SFR_B_EXT_IO_N(0x14A, RX_CTRL, Dummy7, Dummy6, Dummy5, Dummy4, PDT_THRES3, PDT_THRES2, PDT_THRES1, PDT_THRES0)
 SFR_B_EXT_IO_N(0x14B, SFD_VALUE, SFD_VALUE7, SFD_VALUE6, SFD_VALUE5, SFD_VALUE4, SFD_VALUE3, SFD_VALUE2, SFD_VALUE1, SFD_VALUE0)
 SFR_B_EXT_IO_N(0x14C, TRX_CTRL_2, RX_SAFE_MODE, Res4, Res3, Res2, Res1, Res0, OQPSK_DATA_RATE1, OQPSK_DATA_RATE0)
 SFR_B_EXT_IO_N(0x14D, ANT_DIV, ANT_SEL, Res2, Res1, Res0, ANT_DIV_EN, ANT_EXT_SW_EN, ANT_CTRL1, ANT_CTRL0)
 SFR_B_EXT_IO_N(0x14E, IRQ_MASK, AWAKE_EN, TX_END_EN, AMI_EN, CCA_ED_DONE_EN, RX_END_EN, RX_START_EN, PLL_UNLOCK_EN, PLL_LOCK_EN)
 SFR_B_EXT_IO_N(0x14F, IRQ_STATUS, AWAKE, TX_END, AMI, CCA_ED_DONE, RX_END, RX_START, PLL_UNLOCK, PLL_LOCK)
 SFR_B_EXT_IO_N(0x150, VREG_CTRL, AVREG_EXT, AVDD_OK, Dummy5, Dummy4, DVREG_EXT, DVDD_OK, Dummy1, Dummy0)
 SFR_B_EXT_IO_N(0x151, BATMON, BAT_LOW, BAT_LOW_EN, BATMON_OK, BATMON_HR, BATMON_VTH3, BATMON_VTH2, BATMON_VTH1, BATMON_VTH0)
 SFR_B_EXT_IO_N(0x152, XOSC_CTRL, XTAL_MODE3, XTAL_MODE2, XTAL_MODE1, XTAL_MODE0, XTAL_TRIM3, XTAL_TRIM2, XTAL_TRIM1, XTAL_TRIM0)
 SFR_B_EXT_IO_N(0x155, RX_SYN, RX_PDT_DIS, Res2, Res1, Res0, RX_PDT_LEVEL3, RX_PDT_LEVEL2, RX_PDT_LEVEL1, RX_PDT_LEVEL0)
 SFR_B_EXT_IO_N(0x157, XAH_CTRL_1, Res1, Res0, AACK_FLTR_RES_FT, AACK_UPLD_RES_FT, Dummy3, AACK_ACK_TIME, AACK_PROM_MODE, Res)
 SFR_B_EXT_IO_N(0x158, FTN_CTRL, FTN_START, Dummy6, Dummy5, Dummy4, Dummy3, Dummy2, Dummy1, Dummy0)
 SFR_B_EXT_IO_N(0x15A, PLL_CF, PLL_CF_START, Dummy6, Dummy5, Dummy4, Dummy3, Dummy2, Dummy1, Dummy0)
 SFR_B_EXT_IO_N(0x15B, PLL_DCU, PLL_DCU_START, Dummy6, Dummy5, Dummy4, Dummy3, Dummy2, Dummy1, Dummy0)
 SFR_B_EXT_IO_N(0x15C, PART_NUM, PART_NUM7, PART_NUM6, PART_NUM5, PART_NUM4, PART_NUM3, PART_NUM2, PART_NUM1, PART_NUM0)
 SFR_B_EXT_IO_N(0x15D, VERSION_NUM, VERSION_NUM7, VERSION_NUM6, VERSION_NUM5, VERSION_NUM4, VERSION_NUM3, VERSION_NUM2, VERSION_NUM1, VERSION_NUM0)
 SFR_B_EXT_IO_N(0x15E, MAN_ID_0, MAN_ID_07, MAN_ID_06, MAN_ID_05, MAN_ID_04, MAN_ID_03, MAN_ID_02, MAN_ID_01, MAN_ID_00)
 SFR_B_EXT_IO_N(0x15F, MAN_ID_1, MAN_ID_17, MAN_ID_16, MAN_ID_15, MAN_ID_14, MAN_ID_13, MAN_ID_12, MAN_ID_11, MAN_ID_10)
 SFR_B_EXT_IO_N(0x160, SHORT_ADDR_0, SHORT_ADDR_07, SHORT_ADDR_06, SHORT_ADDR_05, SHORT_ADDR_04, SHORT_ADDR_03, SHORT_ADDR_02, SHORT_ADDR_01, SHORT_ADDR_00)
 SFR_B_EXT_IO_N(0x161, SHORT_ADDR_1, SHORT_ADDR_17, SHORT_ADDR_16, SHORT_ADDR_15, SHORT_ADDR_14, SHORT_ADDR_13, SHORT_ADDR_12, SHORT_ADDR_11, SHORT_ADDR_10)
 SFR_B_EXT_IO_N(0x162, PAN_ID_0, PAN_ID_07, PAN_ID_06, PAN_ID_05, PAN_ID_04, PAN_ID_03, PAN_ID_02, PAN_ID_01, PAN_ID_00)
 SFR_B_EXT_IO_N(0x163, PAN_ID_1, PAN_ID_17, PAN_ID_16, PAN_ID_15, PAN_ID_14, PAN_ID_13, PAN_ID_12, PAN_ID_11, PAN_ID_10)
 SFR_B_EXT_IO_N(0x164, IEEE_ADDR_0, IEEE_ADDR_07, IEEE_ADDR_06, IEEE_ADDR_05, IEEE_ADDR_04, IEEE_ADDR_03, IEEE_ADDR_02, IEEE_ADDR_01, IEEE_ADDR_00)
 SFR_B_EXT_IO_N(0x165, IEEE_ADDR_1, IEEE_ADDR_17, IEEE_ADDR_16, IEEE_ADDR_15, IEEE_ADDR_14, IEEE_ADDR_13, IEEE_ADDR_12, IEEE_ADDR_11, IEEE_ADDR_10)
 SFR_B_EXT_IO_N(0x166, IEEE_ADDR_2, IEEE_ADDR_27, IEEE_ADDR_26, IEEE_ADDR_25, IEEE_ADDR_24, IEEE_ADDR_23, IEEE_ADDR_22, IEEE_ADDR_21, IEEE_ADDR_20)
 SFR_B_EXT_IO_N(0x167, IEEE_ADDR_3, IEEE_ADDR_37, IEEE_ADDR_36, IEEE_ADDR_35, IEEE_ADDR_34, IEEE_ADDR_33, IEEE_ADDR_32, IEEE_ADDR_31, IEEE_ADDR_30)
 SFR_B_EXT_IO_N(0x168, IEEE_ADDR_4, IEEE_ADDR_47, IEEE_ADDR_46, IEEE_ADDR_45, IEEE_ADDR_44, IEEE_ADDR_43, IEEE_ADDR_42, IEEE_ADDR_41, IEEE_ADDR_40)
 SFR_B_EXT_IO_N(0x169, IEEE_ADDR_5, IEEE_ADDR_57, IEEE_ADDR_56, IEEE_ADDR_55, IEEE_ADDR_54, IEEE_ADDR_53, IEEE_ADDR_52, IEEE_ADDR_51, IEEE_ADDR_50)
 SFR_B_EXT_IO_N(0x16A, IEEE_ADDR_6, IEEE_ADDR_67, IEEE_ADDR_66, IEEE_ADDR_65, IEEE_ADDR_64, IEEE_ADDR_63, IEEE_ADDR_62, IEEE_ADDR_61, IEEE_ADDR_60)
 SFR_B_EXT_IO_N(0x16B, IEEE_ADDR_7, IEEE_ADDR_77, IEEE_ADDR_76, IEEE_ADDR_75, IEEE_ADDR_74, IEEE_ADDR_73, IEEE_ADDR_72, IEEE_ADDR_71, IEEE_ADDR_70)
 SFR_B_EXT_IO_N(0x16C, XAH_CTRL_0, MAX_FRAME_RETRIES3, MAX_FRAME_RETRIES2, MAX_FRAME_RETRIES1, MAX_FRAME_RETRIES0, MAX_CSMA_RETRIES2, MAX_CSMA_RETRIES1, MAX_CSMA_RETRIES0, SLOTTED_OPERATION)
 SFR_B_EXT_IO_N(0x16D, CSMA_SEED_0, CSMA_SEED_07, CSMA_SEED_06, CSMA_SEED_05, CSMA_SEED_04, CSMA_SEED_03, CSMA_SEED_02, CSMA_SEED_01, CSMA_SEED_00)
 SFR_B_EXT_IO_N(0x16E, CSMA_SEED_1, AACK_FVN_MODE1, AACK_FVN_MODE0, AACK_SET_PD, AACK_DIS_ACK, AACK_I_AM_COORD, CSMA_SEED_12, CSMA_SEED_11, CSMA_SEED_10)
 SFR_B_EXT_IO_N(0x16F, CSMA_BE, MAX_BE3, MAX_BE2, MAX_BE1, MAX_BE0, MIN_BE3, MIN_BE2, MIN_BE1, MIN_BE0)
 SFR_B_EXT_IO_N(0x176, TST_CTRL_DIGI, Dummy7, Dummy6, Dummy5, Dummy4, TST_CTRL_DIG3, TST_CTRL_DIG2, TST_CTRL_DIG1, TST_CTRL_DIG0)
 SFR_B_EXT_IO_N(0x17B, TST_RX_LENGTH, RX_LENGTH7, RX_LENGTH6, RX_LENGTH5, RX_LENGTH4, RX_LENGTH3, RX_LENGTH2, RX_LENGTH1, RX_LENGTH0)
 SFR_B_EXT_IO_N(0x180, TRXFBST, TRXFBST7, TRXFBST6, TRXFBST5, TRXFBST4, TRXFBST3, TRXFBST2, TRXFBST1, TRXFBST0)
 SFR_B_EXT_IO_N(0x1FF, TRXFBEND, TRXFBEND7, TRXFBEND6, TRXFBEND5, TRXFBEND4, TRXFBEND3, TRXFBEND2, TRXFBEND1, TRXFBEND0)
 
#pragma language=restore

#ifndef __IAR_SYSTEMS_ASM__
#include "bitdefinitions/iom128rfa1.inc"
#endif

#endif /* __IOM128RFA1_H (SFR part) */
